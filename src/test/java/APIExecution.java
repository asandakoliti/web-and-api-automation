import core.Base;
import org.junit.*;
import org.junit.rules.TestName;
import resources.Reporting;
import testing.actions.RestAPIActions;

public class APIExecution extends Base {

    @Rule
    public TestName name = new TestName();

    @Before
    public void PreTest()
    {
        TestName = name.getMethodName();
        Reporting.createTest();
    }

    @After
    public void PostTest()
    {
    }

    @Test
    public void API_AllDogs_List_And_Validation()
    {
        String BaseUrl = "https://dog.ceo/api/breeds/list/all";
        String EndPoint = "";

        String APIResult = RestAPIActions.APICall_AndValidation(BaseUrl,EndPoint,"success","retriever");
        Assert.assertTrue("Result Not Good for API Test"+ APIResult,APIResult == null);
    }

    @Test
    public void API_Random_Image()
    {
        String BaseUrl = "https://dog.ceo/api/breeds/image/random";
        String EndPoint = "";

        String APIResult = RestAPIActions.APIRandomImage(BaseUrl,EndPoint,"success");
        Assert.assertTrue("Result Not Good for API Test"+ APIResult,APIResult == null);
    }



}
