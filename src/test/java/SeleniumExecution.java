import core.Base;
import org.junit.*;
import org.junit.rules.TestName;
import resources.Reporting;
import resources.WebTestDriver;
import testing.actions.HomePageActions;
public class SeleniumExecution extends Base
{

    @Rule
    public TestName name = new TestName();

    @Before
    public void PreTest()
    {
        TestName = name.getMethodName();
        Reporting.createTest();

        SeleniumExecutionInstance = new WebTestDriver(WebTestDriver.BrowserType.CHROME);

        //SeleniumExecutionInstance = new WebTestDriver(WebTestDriver.BrowserType.FIREFOX);
    }

    @After
    public void PostTest()
    {
        SeleniumExecutionInstance.shutDown();
    }

    @Test
    public void SeleniumTest()
    {
        String HomePage = HomePageActions.HomePage("FName1", "LName1","AAA","Pass1","Admin","admin@mail.com","082555");
            Assert.assertTrue("Result Not Good"+ HomePage,HomePage == null);

    }
}
