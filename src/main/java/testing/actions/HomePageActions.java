package testing.actions;

import core.Base;
import resources.Reporting;
import testing.Pages.TestPaths;

public class HomePageActions extends Base {

    public static String HomePage (String FirstName, String LastName, String Customer, String Password ,String Role, String Email, String Cell) {

        if (!SeleniumExecutionInstance.Navigate(TestPaths.url())) {
            return Reporting.TestFailed("Failed to Navigate to -  " + TestPaths.url());
        }

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.HomePage())) {
            return Reporting.TestFailed("Failed to Navigate to Home Page for URL - "+ TestPaths.url());
        }

        Reporting.StepPassed("successfully navigated to the Home Page");
        Reporting.StepPassedWithScreenShot("Home Page");

        if (!SeleniumExecutionInstance.ClickElement(TestPaths.HomePage())) {
            return Reporting.TestFailed("Failed to Click on the Add User Button");
        }

        //Add User
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.AddUserPopUp())) {
            return Reporting.TestFailed("Failed to find the new Window for Add Pop Up");
        }

        Reporting.StepPassed("successfully navigated to the Add User Page");
        Reporting.StepPassedWithScreenShot("Add User Window");

        //First Name
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.FirstNameInput())) {
            return Reporting.TestFailed("Failed to find First Name Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.EnterText(TestPaths.FirstNameInput(),FirstName))
        {
            return Reporting.TestFailed( "Failed to Enter the First Name");
        }
        Reporting.StepPassed("successfully entered the First Name");

        //Last Name
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.LastNameInput())) {
            return Reporting.TestFailed("Failed to find Last Name Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.EnterText(TestPaths.LastNameInput(),LastName))
        {
            return Reporting.TestFailed( "Failed to Enter the Last Name");
        }

        Reporting.StepPassed("successfully entered the Last Name");

        //User Name
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.UserNameInput())) {
            return Reporting.TestFailed("Failed to find User Name Input on the pop up window");
        }

        //Generate Unique User Name To length
        String UniqueUserName = SeleniumExecutionInstance.RandomUserName(6);

        if (!SeleniumExecutionInstance.EnterText(TestPaths.UserNameInput(),UniqueUserName))
        {
            return Reporting.TestFailed( "Failed to Enter the Unique User Name");
        }

        Reporting.StepPassed("successfully entered the Unique User Name");

        //Password
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.PasswordInput())) {
            return Reporting.TestFailed("Failed to find User Password Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.EnterText(TestPaths.PasswordInput(),Password))
        {
            return Reporting.TestFailed( "Failed to Enter the User Password");
        }

        Reporting.StepPassed("successfully entered the Unique User Name");

        //Customer AAA Selection
        if(Customer.equalsIgnoreCase("BBB"))
        {
            if (!SeleniumExecutionInstance.WaitForElement(TestPaths.CompanyBBB())) {
                return Reporting.TestFailed("Failed to find Customer BBB Selection");
            }
            if (!SeleniumExecutionInstance.ClickElement(TestPaths.CompanyBBB())) {
                return Reporting.TestFailed("Failed to click Customer BBB Radio Button");
            }

            Reporting.StepPassed("successfully selected the Customer Company as BBB");
        }
        else if (Customer.equalsIgnoreCase("AAA"))
        {
            if (!SeleniumExecutionInstance.WaitForElement(TestPaths.CompanyAAA())) {
                return Reporting.TestFailed("Failed to find Customer AAA Selection");
            }
            if (!SeleniumExecutionInstance.ClickElement(TestPaths.CompanyAAA())) {
                return Reporting.TestFailed("Failed to click Customer AAA Radio Button");
            }

            Reporting.StepPassed("successfully selected the Customer Company as AAA");
        }
        else
         {
                Reporting.warning(" User did not input a valid Customer Company to select");
         }

        //ROLE
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.RoleInput())) {
            return Reporting.TestFailed("Failed to find User Role Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.ClickElement(TestPaths.RoleInput())) {
            return Reporting.TestFailed("Failed to Select User Role Required Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.OptionRoleInput(Role))) {
            return Reporting.TestFailed("Failed to find User Role " + Role+ "on the pop up window");
        }
        if (!SeleniumExecutionInstance.ClickElement(TestPaths.OptionRoleInput(Role))) {
            return Reporting.TestFailed("Failed to Select User Role "+Role+" on the pop up window");
        }

        Reporting.StepPassed("successfully selected the User Role - "+Role);

        //Email
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.EmailInput())) {
            return Reporting.TestFailed("Failed to find User Email Input on the pop up window");
        }

        if (!SeleniumExecutionInstance.EnterText(TestPaths.EmailInput(),Email))
        {
            return Reporting.TestFailed( "Failed to Enter the User Email");
        }

        Reporting.StepPassed("successfully entered the User Email");

        //Cell Phone
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.MobilePhoneInput()))
        {
            return Reporting.TestFailed("Failed to find User Mobile phone Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.EnterText(TestPaths.MobilePhoneInput(),Cell))
        {
            return Reporting.TestFailed( "Failed to Enter the User Mobile phone number");
        }

        Reporting.StepPassed("successfully entered the Mobile phone number");
        Reporting.StepPassedWithScreenShot("successfully completed Add ");

        //SaveButton
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.SaveButton())) {
            return Reporting.TestFailed("Failed to find the Save Button on the pop up window");
        }
        if (!SeleniumExecutionInstance.ClickElement(TestPaths.SaveButton())) {
            return Reporting.TestFailed("Failed to Click on the save button of the pop up window");
        }

        Reporting.StepPassed("successfully Click on the Save Button");

        //Validation
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.HomePage())) {
            return Reporting.TestFailed("Failed to Navigate to Home Page after adding a New User");
        }
        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.SearchValue()))
        {
            return Reporting.TestFailed("Failed to find User Mobile phone Input on the pop up window");
        }
        if (!SeleniumExecutionInstance.EnterText(TestPaths.SearchValue(),UniqueUserName))
        {
            return Reporting.TestFailed( "Failed to Enter the User Mobile phone number");
        }

        if (!SeleniumExecutionInstance.WaitForElement(TestPaths.PageNumber()))
        {
            return Reporting.TestFailed("Failed to validate the searched value returns one search");
        }

        if (!SeleniumExecutionInstance.ValidationElement(TestPaths.FirstNameValidation(),FirstName))
        {
            return Reporting.TestFailed("Failed to validate the First Name");
        }
        Reporting.StepPassed("successfully validated the First Name as - " + FirstName);

        if (!SeleniumExecutionInstance.ValidationElement(TestPaths.LastNameValidation(),LastName))
        {
            return Reporting.TestFailed("Failed to validate the Last Name");
        }
        Reporting.StepPassed("successfully validated the Last Name as - " + LastName);

        if (!SeleniumExecutionInstance.ValidationElement(TestPaths.UserValidation(),UniqueUserName))
        {
            return Reporting.TestFailed("Failed to validate the Unique User Name");
        }
        Reporting.StepPassed("successfully validated the User Name as - " + UniqueUserName);

        if (!SeleniumExecutionInstance.ValidationElement(TestPaths.RoleValidation(),Role))
        {
            return Reporting.TestFailed("Failed to validate the user Role");
        }
        Reporting.StepPassed("successfully validated the User Role as - " + Role);

        if (!SeleniumExecutionInstance.ValidationElement(TestPaths.EmailValidation(),Email))
        {
            return Reporting.TestFailed("Failed to validate the User Email");
        }
        Reporting.StepPassed("successfully validated the User Email as - " + Email);

        if (!SeleniumExecutionInstance.ValidationElement(TestPaths.CellPhoneValidation(),Cell))
        {
            return Reporting.TestFailed("Failed to validate the user Mobile Number" + Cell);
        }
        Reporting.StepPassed("successfully validated the user Mobile Number as - " + Cell);

        Reporting.StepPassed("successfully validated the added user");
        Reporting.StepPassedWithScreenShot("successful Validation");

        return Reporting.finaliseTest();
    }

}
