package testing.Pages;

import org.openqa.selenium.By;

public class TestPaths {

    public static String url() {return "http://www.way2automation.com/angularjs-protractor/webtables/";}

    public static By HomePage() {return By.xpath("//button[text()=' Add User']");}

    public static By AddUserPopUp() {return By.xpath("//h3[text()='Add User']");}

    public static By FirstNameInput() {return By.xpath("//input[@name= 'FirstName']");}

    public static By LastNameInput() {return By.xpath("//input[@name= 'LastName']");}

    public static By UserNameInput() {return By.xpath("//input[@name= 'UserName']");}

    public static By PasswordInput() {return By.xpath("//input[@name= 'Password']");}

    public static By EmailInput() {return By.xpath("//input[@name= 'Email']");}

    public static By CompanyBBB() {return By.xpath("//label[text()='Company BBB']");}

    public static By CompanyAAA() {return By.xpath("//label[text()='Company AAA']");}

    public static By MobilePhoneInput() {return By.xpath("//input[@name= 'Mobilephone']");}

    public static By RoleInput() {return By.xpath("//select[@name= 'RoleId']");}

    public static By OptionRoleInput(String RoleSelection) {return By.xpath("//option[text()= '"+RoleSelection+"']");}

    public static By OptionRoleInput() {return By.xpath("//option[@value= '1']");}

    public static By SaveButton() {return By.xpath("//button[text()='Save']");}

    public static By PageNumber() {return By.xpath("//a[text()='1']");}

    public static By SearchValue() {return By.xpath("//input[@ng-model = 'searchValue']");}

    //Validations
    public static By FirstNameValidation() {return By.xpath("//td[@class='smart-table-data-cell'][1]");}

    public static By LastNameValidation() {return By.xpath("//td[@class='smart-table-data-cell'][2]");}

    public static By UserValidation() {return By.xpath("//td[@class='smart-table-data-cell'][3]");}

    public static By CustomerValidation() {return By.xpath("//input[@ng-model = 'searchValue']");}

    public static By RoleValidation() {return By.xpath("//td[@class='smart-table-data-cell'][5]");}

    public static By EmailValidation() {return By.xpath("//td[@class='smart-table-data-cell'][6]");}

    public static By CellPhoneValidation() {return By.xpath("//td[@class='smart-table-data-cell'][7]");}

}
